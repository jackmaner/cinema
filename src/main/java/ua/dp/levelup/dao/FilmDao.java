package ua.dp.levelup.dao;

import ua.dp.levelup.core.model.Film;

import java.util.List;

public interface FilmDao {

    void createFilm(Film film);

    List<Film> getAllFilms();

    Film getFilmById(long filmId);

    void updateFilm(Film film);

    void deleteFilm(Film film);

    void deleteFilmById(long filmId);
}
