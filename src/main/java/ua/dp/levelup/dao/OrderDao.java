package ua.dp.levelup.dao;

import ua.dp.levelup.core.model.Order;
import ua.dp.levelup.core.model.Ticket;

import java.util.List;

public interface OrderDao {

    void createOrder(Order order, List<Ticket> tickets);

    void updateOrder(Order order);

    Order getOrderById(long orderId);

    List<Order> getAllOrders();

    List<Order> getAllOrdersForToday();
}
