package ua.dp.levelup.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ua.dp.levelup.core.model.Row;
import ua.dp.levelup.dao.RowDao;

import java.util.List;

@Repository
@Transactional
public class RowDaoImpl implements RowDao {

    @Autowired
    private HibernateTemplate template;

    @Override
    public void createRow(Row row) {
        template.save(row);
    }

    @Override
    public List<Row> getAllRows() {
        return template.loadAll(Row.class);
    }

    @Override
    public Row getRowById(Long id) {
        return template.load(Row.class, id);
    }

    @Override
    public void updateRow(Row row) {
        template.update(row);
    }

    @Override
    public void deleteRow(Row row) {
        template.delete(row);
    }
}
