package ua.dp.levelup.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ua.dp.levelup.core.model.Seat;
import ua.dp.levelup.dao.SeatDao;

import java.util.List;

@Repository
@Transactional
public class SeatDaoImpl implements SeatDao {

    @Autowired
    private HibernateTemplate template;

    @Override
    public void createSeat(Seat seat) {
        template.save(seat);
    }

    @Override
    public List<Seat> getAllSeats() {
        return template.loadAll(Seat.class);
    }

    @Override
    public Seat getSeatById(Long id) {
        return template.load(Seat.class, id);
    }

    @Override
    public void updateSeat(Seat seat) {
        template.update(seat);
    }

    @Override
    public void deleteSeat(Seat seat) {
        template.delete(seat);
    }
}
