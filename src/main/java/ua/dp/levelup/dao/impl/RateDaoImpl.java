package ua.dp.levelup.dao.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ua.dp.levelup.core.model.Rate;
import ua.dp.levelup.dao.RateDao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.math.BigInteger;
import java.util.List;

/**
 * Created by andre on 01.08.2017.
 */
@Repository
@Transactional
public class RateDaoImpl implements RateDao {

    @Autowired
    private HibernateTemplate template;


    private EntityManager entityManager;

    @PersistenceContext(unitName = "emf")
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void createRate(Rate rate) {
        template.save(rate);
    }

    @Override
    public List<Rate> getAllRates() {
        return template.loadAll(Rate.class);
    }

    @Override
    public Rate getRateById(Long id) {
        return template.load(Rate.class, id);
    }

    @Override
    public void updateRate(Rate rate) {
        template.update(rate);
    }

    @Override
    public void deleteRate(Rate rate) {
        template.delete(rate);
    }

    @Override
    public Rate getRatesByUserAndFilmId(Long userId, Long filmId) {
        CriteriaBuilder builder = entityManager.getEntityManagerFactory().getCriteriaBuilder();
        CriteriaQuery<Rate> criteriaQuery = builder.createQuery(Rate.class);
        Root<Rate> root = criteriaQuery.from(Rate.class);

        criteriaQuery.select(root);
        criteriaQuery.where(builder.and(builder.equal(root.get("userId"), userId), builder.equal(root.get("filmId"), filmId)));

        return entityManager.createQuery(criteriaQuery).getSingleResult();
    }

    @Override
    public double getAverageRatingForFilm(long filmId) {
        String rateExistsQuery = String.format("SELECT exists(select * from RATES where film_id = %d)", filmId);
        BigInteger rateExists = (BigInteger) entityManager.createNativeQuery(rateExistsQuery).getSingleResult();
        if (rateExists.intValue() > 0) {
            String hqlQuery = String.format("select avg(value) from Rate rt where rt.filmId = %d", filmId);
            return (double) entityManager.createQuery(hqlQuery).getSingleResult();
        }

        return 0;
    }
}
