package ua.dp.levelup.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ua.dp.levelup.core.model.MovieSession;
import ua.dp.levelup.dao.MovieSessionDao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Date;
import java.util.List;

/**
 * @author Alexandr Shegeda on 23.06.17.
 */
@Repository
@Transactional
public class MovieSessionDaoImpl implements MovieSessionDao {

    @Autowired
    private HibernateTemplate template;
    private EntityManager entityManager;

    @PersistenceContext(unitName = "emf")
//  @Qualifier(value = "emf")
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Value(value = "${my.value:default-value}")
    private String value;

    @Override
    public void createMovieSession(MovieSession session) {
        template.save(session);
    }

    @Override
    public MovieSession getMovieSessionById(long sessionId) {
        return template.get(MovieSession.class, sessionId);
    }

    @Override
    public List<MovieSession> getAllMovieSessions() {
        return template.loadAll(MovieSession.class);
    }

    @Override
    public List<MovieSession> getMovieSessionsForToday() {
        return getAllMovieSessionByDate(new Date());
    }

    @Override
    public List<MovieSession> getAllMovieSessionByDate(Date date) {
        CriteriaBuilder builder = entityManager.getEntityManagerFactory().getCriteriaBuilder();
        CriteriaQuery<MovieSession> criteriaQuery = builder.createQuery(MovieSession.class);
        Root<MovieSession> root = criteriaQuery.from(MovieSession.class);

        criteriaQuery.select(root);
        criteriaQuery.where(builder.equal(root.get("sessionStartDate"), date));

        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public List<MovieSession> getMovieSessionsByDate(Date date) {
        String query = "from MovieSession ms where ms.sessionStartDate = :movieSessionDate";
        return (List<MovieSession>) template.findByNamedParam(query, "movieSessionDate", date);
    }
}
