package ua.dp.levelup.dao.impl;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ua.dp.levelup.core.model.Film;
import ua.dp.levelup.dao.FilmDao;

import java.util.List;

@Repository
@Transactional
public class FilmDaoImpl implements FilmDao {

    @Autowired
    private HibernateTemplate hibernateTemplate;

    @Autowired
    public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
        this.hibernateTemplate = hibernateTemplate;
    }

    @Override
    public void createFilm(Film film) {
        hibernateTemplate.save(film);
    }

    @Override
    public List<Film> getAllFilms() {
        return hibernateTemplate.loadAll(Film.class);
    }

    @Override
    public Film getFilmById(long filmId) {
        Film film = hibernateTemplate.load(Film.class, filmId);
        Hibernate.initialize(film.getSessionList());
        return film;
    }

    @Override
    public void deleteFilm(Film film) {
        hibernateTemplate.delete(film);
    }

    @Override
    public void deleteFilmById(long filmId) {
        Film filmToDelete = getFilmById(filmId);
        if (null != filmToDelete) {
            hibernateTemplate.delete(filmToDelete);
        }
    }

    @Override
    public void updateFilm(Film film) {
        hibernateTemplate.update(film);
    }
}
