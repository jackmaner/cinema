package ua.dp.levelup.dao;

import ua.dp.levelup.core.model.Hall;

import java.util.List;

public interface HallDao {
    void createHall(Hall hall);

    void updateHall(Hall hall);

    void deleteHall(Hall hall);

    Hall getHallById(Long id);

    Hall getHallByNumber(int hallNumber);

    List<Hall> getAllHalls();

}
