package ua.dp.levelup.dao;

import ua.dp.levelup.core.model.Rate;

import java.util.List;

public interface RateDao {
    void createRate(Rate rate);

    List<Rate> getAllRates();

    Rate getRateById(Long id);

    void updateRate(Rate rate);

    void deleteRate(Rate rate);

    Rate getRatesByUserAndFilmId(Long userId, Long filmId);

    double getAverageRatingForFilm(long filmId);
}
