package ua.dp.levelup.core.model.message;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by java on 28.07.2017.
 */

@Getter
@Setter
public class EmailText extends MessageForUsers {

    private String messageText;

    public EmailText(String recipient, String subject, String messageText) {
        super(recipient, subject);
        this.messageText = messageText;
    }
}
