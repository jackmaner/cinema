package ua.dp.levelup.core.model.app;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import ua.dp.levelup.core.model.Film;
import ua.dp.levelup.core.model.User;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@ToString(exclude = {"film", "user"})
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "COMMENTS")
@JsonIgnoreProperties({"film"})
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long commentId;

    private String text;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "film_id", nullable = false)
    private Film film;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date dateComment;
}