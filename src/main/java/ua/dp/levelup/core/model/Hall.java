package ua.dp.levelup.core.model;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@ToString(exclude = "rows")
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "HALLS")
public class Hall {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long hallId;

    @Column(nullable = false, unique = true)
    private int hallNumber;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "hall")
    private List<Row> rows;

    public Hall(int hallNumber) {
        this.hallNumber = hallNumber;
    }

    public Hall(int hallNumber, List<Row> rows) {
        this.hallNumber = hallNumber;
        this.rows = rows;
    }
}