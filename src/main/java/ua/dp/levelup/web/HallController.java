package ua.dp.levelup.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ua.dp.levelup.core.model.Hall;
import ua.dp.levelup.service.HallService;
import ua.dp.levelup.service.RowService;
import ua.dp.levelup.service.SeatService;

import java.util.List;

@Controller
@RequestMapping("/hall")
public class HallController {

    @Autowired
    private HallService hallService;

    @Autowired
    private RowService rowService;

    @Autowired
    private SeatService seatService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ModelAndView getAllHalls() {
        List<Hall> halls = hallService.getAllHalls();
        ModelAndView modelAndView = new ModelAndView("management-halls");
        modelAndView.addObject("halls", halls);
        return modelAndView;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String createHall(@RequestBody Hall hall) {
        hallService.createHall(hall);
        rowService.createListRows(hall.getRows(), hall);
        seatService.createAllSeats(hall.getRows());

        return "redirect:/hall";
    }
}
