package ua.dp.levelup.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/tiles")
public class TilesController {


    @RequestMapping("/main")
    public ModelAndView mainPage() {
        return new ModelAndView("mainTemplate");
    }
}
