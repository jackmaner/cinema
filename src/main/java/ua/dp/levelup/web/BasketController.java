package ua.dp.levelup.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ua.dp.levelup.core.model.MovieSession;
import ua.dp.levelup.core.model.Ticket;
import ua.dp.levelup.service.MovieSessionService;
import ua.dp.levelup.service.UserService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by unike on 31.07.2017.
 */

@Controller
@RequestMapping("/basket")
public class BasketController {

    @Autowired
    private MovieSessionService movieSessionService;

    @Autowired
    private UserService userService;

    Map<Long, Map<Long, List<Ticket>>> allTicketsOfClient = new HashMap<>();
    Long clientId = 1L;

    @RequestMapping(value = "/test", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity test(@RequestBody List<Ticket> tickets) {
        for (Ticket t : tickets) {
            System.out.println(t.toString());
        }
        if (allTicketsOfClient.get(clientId) == null) {
            allTicketsOfClient.put(clientId, new HashMap<>());
            allTicketsOfClient.get(clientId).put(tickets.get(0).getMovieSessionId(), tickets);
        } else {
            allTicketsOfClient.get(clientId).remove(tickets.get(0).getMovieSessionId());
            allTicketsOfClient.get(clientId).put(tickets.get(0).getMovieSessionId(), tickets);
        }
        return new ResponseEntity(HttpStatus.OK);
    }


    @RequestMapping(value = "/")
    public ModelAndView basket() {
        ModelAndView modelAndView = new ModelAndView("view-basket");
        Map<Long, List<Ticket>> allTicketsBymvisessionsIDsOfClient = allTicketsOfClient.get(clientId);
        Set<Long> seansesIDs = allTicketsBymvisessionsIDsOfClient.keySet();
        List<MovieSession> allMovieSessions = movieSessionService.getAllMovieSessions();
        modelAndView.addObject("clientTickets", allTicketsBymvisessionsIDsOfClient);
        modelAndView.addObject("seansesIDs", seansesIDs);
        modelAndView.addObject("allMovieSessions", allMovieSessions);

        /////ждем димона пока допилит логин
        System.out.println("///////////////////////////////////////////////////");
        System.out.println(SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        /////ждем димона пока допилит логин


        return modelAndView;
    }

    @RequestMapping(value = "/buy", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity buy(@RequestBody List<Ticket> tickets) {
        for (Ticket t : tickets) {
            System.out.println("test of buy " + t.toString());
        }

        allTicketsOfClient.put(clientId, new HashMap<>());
        return new ResponseEntity(HttpStatus.OK);
    }

    /////ждем димона пока допилит логин
    @RequestMapping(value = "/get-user-id", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity getUserIdByEmail(@RequestBody User user) {
        User userByEmail = userService.getUserByEmail(user.getEmail());
        System.out.println(userByEmail);
        if (userByEmail == null) return new ResponseEntity(HttpStatus.BAD_REQUEST);
        else return new ResponseEntity(HttpStatus.OK);
    }

    long getCurrentUserId() {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return user.getUserId();
    }
    /////ждем димона пока допилит логин

}
