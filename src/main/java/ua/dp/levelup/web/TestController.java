package ua.dp.levelup.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@Controller
public class TestController {


    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public ModelAndView getUserInfo(HttpSession session) {
        return new ModelAndView("user-lang");
    }
}
