package ua.dp.levelup.web;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ua.dp.levelup.core.model.Rate;
import ua.dp.levelup.service.RateService;

/**
 * Created by andre on 04.08.2017.
 */
@Controller
@RequestMapping("/rate")
public class RateController {

    public static final Logger LOGGER = Logger.getLogger(RateController.class);

    @Autowired
    private RateService rateService;

    @ResponseBody
    @RequestMapping(value = "/getRating", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity addUserFromRegistration(@RequestBody Rate rate) {
        LOGGER.info(rate);

        rateService.updateRate(rate);

        double avgRate = rateService.getAverageRatingForFilm(rate.getFilmId());
        return new ResponseEntity(avgRate, HttpStatus.OK);
    }
}
