package ua.dp.levelup.web.filters;

import org.apache.log4j.Logger;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CustomFilter extends GenericFilterBean {

    private static final Logger LOGGER = Logger.getLogger(CustomFilter.class);

    @Override
    public void doFilter(
            ServletRequest request,
            ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        LOGGER.info("CustomFilter filtered request: " + httpRequest.getServletPath());
        ((HttpServletResponse) response).addHeader("Lucky", "true");
        chain.doFilter(request, response);
    }


}
