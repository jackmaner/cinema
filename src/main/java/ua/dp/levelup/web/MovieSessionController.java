package ua.dp.levelup.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ua.dp.levelup.core.model.*;
import ua.dp.levelup.core.model.dto.MovieSessionDto;
import ua.dp.levelup.service.EmailManagementService;
import ua.dp.levelup.service.HallService;
import ua.dp.levelup.service.MovieSessionService;
import ua.dp.levelup.service.TicketService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/movie")
public class MovieSessionController {

    @Autowired
    private MovieSessionService movieSessionService;

    @Autowired
    private HallService hallService;

    @Autowired
    private TicketService ticketService;

    @Autowired
    EmailManagementService emailManagementService;

    @RequestMapping(value = "/add-session", method = RequestMethod.GET)
    public ModelAndView getAddMovieSessionPage() {
        ModelAndView modelAndView = new ModelAndView("add-movie-session");
        modelAndView.addObject("session", new MovieSession());
        modelAndView.addObject("pageTitle", "movie.add-session.page.title");
        return modelAndView;
    }

    @RequestMapping(value = "/add-session", method = RequestMethod.POST, consumes = "application/json")
    public String addMovieSession(@RequestBody MovieSession session) {
        movieSessionService.createMovieSession(session);

        return "redirect:list";
    }

    @RequestMapping("/list")
    public ModelAndView getAllMovieSessions() {
        List<MovieSession> allMovieSessions = movieSessionService.getAllMovieSessions();
        ModelAndView modelAndView = new ModelAndView("movie-session-page");

        modelAndView.addObject("allMovieSessions", allMovieSessions);
        modelAndView.addObject("pageTitle", "movie.list.page.title");

        return modelAndView;
    }

    @RequestMapping("/list/{date}")
    public ModelAndView getMovieSessionsByDate(@PathVariable("date") Date dateSessions) {
        List<MovieSession> movieSessionsByDate = movieSessionService.getMovieSessionsByDate(dateSessions);
        ModelAndView modelAndView = new ModelAndView("movie-session-today-page");
        modelAndView.addObject("allMovieSessionsToday", movieSessionsByDate);
        modelAndView.addObject("pageTitle", "movie.list.date.page.title");
        return modelAndView;
    }

    @RequestMapping(value = "/session/{sessionId}")
    public ModelAndView viewMovieSession(@PathVariable Long sessionId) {
        ModelAndView modelAndView = new ModelAndView("view-session");
        modelAndView.addObject("pageTitle", "movie.session");
        List<Ticket> ticketsOfMovieSession = ticketService.getTicketsByMovieSessionId(sessionId);
        MovieSession movieSession = movieSessionService.getMovieSessionById(sessionId);
        int hallNumber = movieSession.getHallNumber();
        Hall hall = hallService.getHallByNumber(hallNumber);
        modelAndView.addObject("hall", hall);
        modelAndView.addObject("tickets", ticketsOfMovieSession);
        modelAndView.addObject("standartPrice", (int) movieSession.getStandardTicketPrice());
        modelAndView.addObject("comfortPrice", (int) movieSession.getComfortTicketPrice());
        modelAndView.addObject("movieSession", movieSession);
        modelAndView.addObject("order", new Order(1L, null, 0, 0L, null));
        return modelAndView;

    }

    @RequestMapping("/session/list")
    public ModelAndView getAllMovieSession(@DateTimeFormat(pattern = "MM/dd/yyyy") @RequestParam(name = "date", required = false) Date date) {
        if (date == null) {
            date = new Date();
        }

        List<MovieSessionDto> allMovieSessions = movieSessionService.getAllMovieSessionByDate(date);
        ModelAndView modelAndView = new ModelAndView("movie-session");
        modelAndView.addObject("pageTitle", "movie.session.list");
        modelAndView.addObject("allMovieSessions", allMovieSessions);

        return modelAndView;
    }


}
