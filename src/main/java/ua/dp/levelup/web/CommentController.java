package ua.dp.levelup.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ua.dp.levelup.core.model.Film;
import ua.dp.levelup.core.model.app.Comment;
import ua.dp.levelup.service.CommentService;
import ua.dp.levelup.service.FilmService;

import java.util.List;

@Controller
public class CommentController {

    @Autowired
    private CommentService commentService;

    @Autowired
    private FilmService filmService;

    @MessageMapping("/chat-room/{id}")
    @SendTo("/topic/comments/{id}")
    public Comment send(@DestinationVariable Long id, Comment comment) throws Exception {
        commentService.createComment(comment);
        return comment;
    }

    @ResponseBody
    @RequestMapping(value = "/film/{id}", headers = "Accept=application/json", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Comment> getPageComments(@RequestBody int pageNumber, @PathVariable Long id) {

        Film film = filmService.getFilmById(id);
        Pageable pageable = commentService.createPageRequest(pageNumber);
        return commentService.findByFilm(film, pageable);
    }
}