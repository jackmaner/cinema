package ua.dp.levelup.service;

import ua.dp.levelup.core.model.Row;
import ua.dp.levelup.core.model.Seat;

import java.util.List;

public interface SeatService {
    void createSeat(Seat seat);

    List<Seat> getAllSeats();

    Seat getSeatById(Long id);

    void updateSeat(Seat seat);

    void deleteSeat(Seat seat);

    void createAllSeats(List<Row> rows);
}