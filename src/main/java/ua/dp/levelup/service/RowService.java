package ua.dp.levelup.service;

import ua.dp.levelup.core.model.Hall;
import ua.dp.levelup.core.model.Row;

import java.util.List;

public interface RowService {
    void createRow(Row row);

    List<Row> getAllRows();

    Row getRowById(Long id);

    void updateRow(Row row);

    void deleteRow(Row row);

    void createListRows(List<Row> rows, Hall hall);
}
