package ua.dp.levelup.service.processors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ua.dp.levelup.service.EmailManagementService;
import ua.dp.levelup.service.message.EmailConsumer;

@Service
public class EmailProcessor {

    private static final Logger LOGGER = Logger.getLogger(EmailProcessor.class);

    @Autowired
    EmailManagementService emailManagementService;

    @Scheduled(cron = "0 0/5 * * * *")
    public void processMessageQueue() {
        Thread thread = new EmailConsumer(emailManagementService);
        thread.start();
        LOGGER.info(String.format("EmailConsumer thread with id: %d was started", thread.getId()));
    }
}
