package ua.dp.levelup.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dp.levelup.core.model.Hall;
import ua.dp.levelup.core.model.Row;
import ua.dp.levelup.dao.RowDao;
import ua.dp.levelup.service.RowService;

import java.util.List;

@Service
public class RowServiceImpl implements RowService {

    private RowDao rowDao;

    @Autowired
    public void setRowDao(RowDao rowDao) {
        this.rowDao = rowDao;
    }

    @Override
    public void createRow(Row row) {
        rowDao.createRow(row);
    }

    @Override
    public List<Row> getAllRows() {
        return rowDao.getAllRows();
    }

    @Override
    public Row getRowById(Long id) {
        return rowDao.getRowById(id);
    }

    @Override
    public void updateRow(Row row) {
        rowDao.updateRow(row);
    }

    @Override
    public void deleteRow(Row row) {
        rowDao.deleteRow(row);
    }

    @Override
    public void createListRows(List<Row> rows, Hall hall) {
        for (Row row : rows) {
            row.setHall(hall);
            createRow(row);
        }
    }
}
