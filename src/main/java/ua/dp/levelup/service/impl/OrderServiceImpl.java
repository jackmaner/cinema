package ua.dp.levelup.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ua.dp.levelup.core.model.Order;
import ua.dp.levelup.core.model.Ticket;
import ua.dp.levelup.core.model.User;
import ua.dp.levelup.core.model.message.EmailOrder;
import ua.dp.levelup.dao.OrderDao;
import ua.dp.levelup.service.EmailManagementService;
import ua.dp.levelup.service.OrderService;
import ua.dp.levelup.service.UserService;

import java.util.*;

@Service("orderService")
public class OrderServiceImpl implements OrderService {

    private static final Logger LOGGER = Logger.getLogger(MovieSessionServiceImpl.class);
    private OrderDao orderDao;
    private Map<Date, Double> dailyProfitsMap = new HashMap<>();
    private EmailManagementService emailManagementService;
    private UserService userService;

    @Autowired
    public void setOrderDao(final OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    @Autowired
    public void setEmailManagementService(EmailManagementService emailManagementService) {
        this.emailManagementService = emailManagementService;
    }

    @Override
    public void createOrder(Ticket... tickets) {
        Order order = new Order();
        for (Ticket t : tickets) {
            order.addTicket(t);
        }
        orderDao.createOrder(order, Arrays.asList(tickets));

        User user = userService.getUserById(order.getClientId());
        emailManagementService.addMessageToQueue(new EmailOrder(user.getEmail(), "cinema: tickets info", order));
    }

    @Override
    public void updateOrder(Order order) {
        orderDao.updateOrder(order);
    }

    @Override
    public List<Order> getAllOrders() {
        return orderDao.getAllOrders();
    }

    @Override
    public Order getOrderById(long orderId) {
        return orderDao.getOrderById(orderId);
    }

    @Override
    public List<Order> getAllOrdersForToday() {
        return orderDao.getAllOrdersForToday();
    }

    @Override
    public double getDailyProfit() {
        double dailyProfit = 0D;
        List<Order> ordersForToday = orderDao.getAllOrdersForToday();
        for (Order order : ordersForToday) {
            dailyProfit += order.getTotalPrice();
        }
        return dailyProfit;
    }

    //Every day at 23:59
    @Scheduled(cron = "0 59 23 * * *")
    private void updateDailyProfitForToday() {

        LOGGER.debug(">>> updateDailyProfitForToday triggered");
        dailyProfitsMap.put(new Date(), getDailyProfit());
    }

    @Override
    public Integer getDailyAmount() {
        List<Order> ordersForToday = orderDao.getAllOrdersForToday();

        int count = 0;
        for (Order order : ordersForToday) {
            count += order.getTickets().size();
        }
        return count;
    }

    @Scheduled(cron = "0 59 23 * * *")
    private void updateDailyAmountForToday() {
        LOGGER.debug(">>> updateDailySalesTicketsForToday triggered");
        System.out.println("Today sales: " + getDailyAmount() + " tickets");
    }
}
