package ua.dp.levelup.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ua.dp.levelup.core.model.Order;
import ua.dp.levelup.core.model.message.MessageForUsers;
import ua.dp.levelup.service.EmailManagementService;

import javax.annotation.PostConstruct;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

import static javax.mail.Message.RecipientType.TO;
import static org.springframework.http.MediaType.TEXT_HTML_VALUE;

@Service
public class EmailManagementServiceImpl implements EmailManagementService {

    private static final Logger LOGGER = Logger.getLogger(MovieSessionServiceImpl.class);
    private static final String TICKET_SOLD_NOTIFICATION_TEMPLATE = "emailNotificationForm";

    @Value("${email.login}")
    private String login;
    @Value("${email.password}")
    private String password;
    @Value("${email.smtp}")
    private String smtp;
    @Value("${email.port}")
    private String port;
    @Value("${email.templates.folder:C:\\apache-tomcat-8.5.16\\webapps\\cinema\\WEB-INF\\classes\\templates}")
    private String emailTemplatesFolder;


    private Session session;

    @Value("${message.queue.size}")
    private int messageQueueSize;

    private volatile Queue<MessageForUsers> queueMessages;

    @PostConstruct
    public void init() {
        Properties props = new Properties();
        props.put("mail.smtp.host", smtp);
        props.put("mail.smtp.socketFactory.port", port);
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", port);
        session = Session.getDefaultInstance(props, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(login, password);
            }
        });
        LOGGER.info(">>> EmailManagementService.init() email session configured");
        queueMessages = new ArrayBlockingQueue<>(messageQueueSize);
    }

    @Override
    public void sendEmail(String recipient, String subject, String emailMessage) throws MessagingException {
        Message message = prepareMessageWithRecipientAndSubject(recipient, subject);
        message.setText(emailMessage);
        Transport.send(message);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(">>> EmailManagementService.sendEmail() to " + recipient + " completed");
        }
    }

    @Override
    public void sendNotificationEmailAfterTicketBuy(String recipient, Order order) throws MessagingException, IOException {
        MimeMessage message = prepareMessageWithRecipientAndSubject(recipient, "JavaLevelUp Cinema - tickets purchase");
        String template = loadHtmlTemplate(TICKET_SOLD_NOTIFICATION_TEMPLATE);
        String content = String.format(template, order.getOrderId(), order.getTotalPrice());
        message.setContent(content, TEXT_HTML_VALUE);
        Transport.send(message);
        LOGGER.info(String.format("Sent message successfully to: %s", recipient));
    }

    private MimeMessage prepareMessageWithRecipientAndSubject(final String recipient, final String subject) throws MessagingException {
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(login));
        message.addRecipient(TO, new InternetAddress(recipient));
        message.setSubject(subject);
        return message;
    }

    private String loadHtmlTemplate(final String templateCode) throws IOException {
        File file = new File(String.format("%s/%s.html", emailTemplatesFolder, templateCode));
        if (!file.exists())
            throw new FileNotFoundException(String.format("HTML template by code %s is not found", templateCode));
        return new String(Files.readAllBytes(Paths.get(file.toURI())));
    }

    @Override
    public Queue<MessageForUsers> getMessageQueue() {
        return queueMessages;
    }

    @Override
    public void addMessageToQueue(MessageForUsers messages) {
        queueMessages.offer(messages);
    }
}


