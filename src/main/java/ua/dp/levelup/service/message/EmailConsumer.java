package ua.dp.levelup.service.message;

import org.apache.log4j.Logger;
import ua.dp.levelup.core.model.message.EmailOrder;
import ua.dp.levelup.core.model.message.EmailText;
import ua.dp.levelup.core.model.message.MessageForUsers;
import ua.dp.levelup.service.EmailManagementService;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.Queue;

public class EmailConsumer extends Thread {

    private static final Logger LOGGER = Logger.getLogger(EmailConsumer.class);

    private final EmailManagementService emailManagementService;

    public EmailConsumer(EmailManagementService emailManagementService) {
        this.emailManagementService = emailManagementService;
    }

    @Override
    public void run() {
        Queue<? extends MessageForUsers> queueMessages = emailManagementService.getMessageQueue();
        MessageForUsers messageForUsers;

        //One thread turns in a queue until it becomes empty
        while ((messageForUsers = queueMessages.poll()) != null) {
            try {
                if (messageForUsers instanceof EmailOrder) {
                    EmailOrder emailOrder = (EmailOrder) messageForUsers;
                    emailManagementService.sendNotificationEmailAfterTicketBuy(emailOrder.getRecipient(), emailOrder.getOrder());
                    LOGGER.info("Executed sendNotificationEmailAfterTicketBuy(). Message sent successfully from " + emailOrder.getRecipient());

                } else if (messageForUsers instanceof EmailText) {
                    EmailText emailText = (EmailText) messageForUsers;
                    emailManagementService.sendEmail(emailText.getRecipient(), emailText.getSubject(), emailText.getMessageText());
                    LOGGER.info("Executed sendEmail(). Message sent successfully from " + emailText.getRecipient());
                }
            } catch (MessagingException | IOException e) {
                LOGGER.error(e);
            }
        }
    }


}
