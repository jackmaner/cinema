package ua.dp.levelup.utils;

import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class CinemaUtilityFunctions {
    public boolean isSameDay(Date date, Date dateToCompare) {
        SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
        return fmt.format(date).equals(fmt.format(dateToCompare));
    }

    public List<Date> getFilterDays() {
        long msInDay = 86400000; //количество миллисекунд в 1 дне
        long msInToday = new Date().getTime();
        List<Date> dateList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            dateList.add(i, new Date(msInToday + (i * msInDay)));
        }
        return dateList;
    }

    public String formatDateForImage() {
        DateFormat fmt = new SimpleDateFormat("yyyyMMddHHmmss");
        return fmt.format(new Date());
    }

    public String generateConfirmCode() {
        //int englishLettersCount = 25;
        //int ascii = 97;
        char[] englishLetters = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
        int codeLenght = 20;
        char[] randomChars = new char[codeLenght];
        for (int i = 0; i < randomChars.length; i++) {
            //randomChars[i] = (char) (ascii + (int) (Math.random() * englishLettersCount));
            randomChars[i] = englishLetters[(int) (Math.random() * englishLetters.length)];
        }
        return new String(randomChars);
    }
}
