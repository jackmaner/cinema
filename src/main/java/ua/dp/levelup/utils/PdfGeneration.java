package ua.dp.levelup.utils;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;


/**
 * Created by Алексей on 28.07.2017.
 */
public class PdfGeneration {

    public static void main(String[] args) throws FileNotFoundException, DocumentException {
        System.out.println(pdfGeneration(2));
    }

    public static PdfWriter pdfGeneration(long ticketId) throws FileNotFoundException, DocumentException {

        Document document = new Document(PageSize.A4, 50, 50, 50, 50);

        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("ticket " + ticketId + ".pdf"));

        document.open();

        document.add(qrCodeGeneration());
        document.add(createContactsInformationAboutCinema("Dafi", "Shevchenko street", "+5555"));
        document.add(createNameOfFilm("Harry Potter"));
        document.add(createInformationAboutTimeOfFilm("14.08.17", 14.15));
        document.add(createInformationAboutSeat(3, 14, 23));
        document.add(createInformationAboutTicket(120, "GOOD", 1545));
        document.add(createPostAboutPolice());

        document.close();

        return writer;
    }

    public static PdfPTable createContactsInformationAboutCinema(String nameOfCinema, String addressOfCinema,
                                                                 String telephoneNumber) throws DocumentException {
        Paragraph cinemaLogo = new Paragraph("Logo: ");// TODO: logoOfCinema
        Paragraph cinemaContacts = new Paragraph("Cinema: " + nameOfCinema + "\n\r" + "Address: " + addressOfCinema
                + "\n\r" + "Telephone: " + telephoneNumber);

        Paragraph importantNotice = new Paragraph("");

        PdfPTable headerTable = new PdfPTable(3);
        float[] columnWidths = {250, 250, 250};
        headerTable.setWidths(columnWidths);
        headerTable.addCell(createPdfPCell(1, Element.ALIGN_LEFT, cinemaLogo));
        headerTable.addCell(createPdfPCell(2, cinemaContacts));
        headerTable.addCell(createPdfPCell(3, importantNotice));
        return headerTable;
    }

    public static Paragraph createNameOfFilm(String nameOfFilm) {

        Font font2 = new Font(Font.FontFamily.HELVETICA, 25, Font.BOLD, new CMYKColor(0, 0, 0, 255));

        Anchor anchorTarget = new Anchor(nameOfFilm, font2);
        anchorTarget.setName("BackToTop");
        Paragraph nameFilm = new Paragraph();

        nameFilm.setAlignment(Element.ALIGN_LEFT);
        nameFilm.setSpacingBefore(50);
        nameFilm.setSpacingAfter(50);
        nameFilm.add(anchorTarget);

        return nameFilm;
    }

    public static PdfPTable createInformationAboutTimeOfFilm(String dateOfFilm, double timeOfFilm) throws DocumentException {

        Font font1 = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD, new CMYKColor(0, 0, 0, 255));

        Paragraph dateFilm = new Paragraph("Date:" + "\n\r" + dateOfFilm, font1);
        Paragraph timeFilm = new Paragraph("Time:" + "\n\r" + timeOfFilm, font1);

        PdfPTable bodyTable = new PdfPTable(3);

        float[] columnWidths1 = {34, 34, 34};

        bodyTable.setWidths(columnWidths1);
        bodyTable.addCell(createPdfPCell(1, Element.ALIGN_LEFT, dateFilm));
        bodyTable.addCell(createPdfPCell(2, timeFilm));
        bodyTable.setSpacingBefore(25);
        bodyTable.setSpacingAfter(25);

        return bodyTable;
    }

    public static PdfPTable createInformationAboutSeat(int hallNumber, int rowNumber, int seatNumber)
            throws DocumentException {

        Font font1 = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD, new CMYKColor(0, 0, 0, 255));

        Paragraph numberOfHall = new Paragraph("Hall:" + "\n\r" + hallNumber, font1);
        Paragraph numberOfRow = new Paragraph("Row:" + "\n\r" + rowNumber, font1);
        Paragraph numberOfSeat = new Paragraph("Seat:" + "\n\r" + seatNumber, font1);

        return createTableWithThreeParagraphsOfSeat(numberOfHall, numberOfRow, numberOfSeat);
    }

    public static PdfPTable createInformationAboutTicket(long ticketCost, String seatCategory, long orderNumber)
            throws DocumentException {
        Font font1 = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD, new CMYKColor(0, 0, 0, 255));

        Paragraph costOfTicket = new Paragraph("Cost:" + "\n\r" + ticketCost, font1);
        Paragraph categoryOfSeat = new Paragraph("Category:" + "\n\r" + seatCategory, font1);
        Paragraph numberOfOrder = new Paragraph("Order number:" + "\n\r" + orderNumber, font1);

        return createTableWithThreeParagraphsOfTicket(costOfTicket, categoryOfSeat, numberOfOrder);
    }

    public static PdfPTable createTableWithThreeParagraphsOfSeat(Paragraph numberOfHall, Paragraph numberOfRow, Paragraph numberOfSeat)
            throws DocumentException {
        PdfPCell numberOfHallCell = createPdfPCell(1, Element.ALIGN_LEFT, numberOfHall);
        PdfPCell numberOfRowCell = createPdfPCell(2, numberOfRow);
        PdfPCell numberOfSeatCell = createPdfPCell(3, numberOfSeat);

        return createPdfPTable(numberOfHallCell, numberOfRowCell, numberOfSeatCell);
    }

    public static PdfPTable createTableWithThreeParagraphsOfTicket(Paragraph costOfTicket, Paragraph categoryOfSeat, Paragraph numberOfOrder)
            throws DocumentException {
        PdfPCell costOfTicketCell = createPdfPCell(1, Element.ALIGN_LEFT, costOfTicket);
        PdfPCell categoryOfSeatCell = createPdfPCell(2, categoryOfSeat);
        PdfPCell numberOfOrderCell = createPdfPCell(3, numberOfOrder);

        return createPdfPTable(costOfTicketCell, categoryOfSeatCell, numberOfOrderCell);
    }

    public static Image qrCodeGeneration() throws BadElementException {

        BarcodeQRCode my_code = new BarcodeQRCode("https://trello.com/b/6jj6D0xL/cinema", 125, 125, null);
        Image qr_image = my_code.getImage();
        qr_image.setAbsolutePosition(400, 685);

        return qr_image;
    }

    public static Paragraph createPostAboutPolice() {

        Font font3 = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD, new CMYKColor(0, 255, 255, 17));

        Paragraph elements = new Paragraph("Buying a ticket you agree to automatically follow the rules and agreements" +
                " of the cinema policy.", font3);

        return elements;
    }

    public static PdfPCell createPdfPCell(int colspanId, int alignment, Paragraph nameOfColumn) {

        PdfPCell pdfPCell = new PdfPCell(nameOfColumn);
        pdfPCell.setColspan(colspanId);
        pdfPCell.setHorizontalAlignment(alignment);
        pdfPCell.setBorder(PdfPCell.NO_BORDER);

        return pdfPCell;
    }

    public static PdfPCell createPdfPCell(int colspanId, Paragraph nameOfColumn) {

        PdfPCell pdfPCell = new PdfPCell(nameOfColumn);
        pdfPCell.setColspan(colspanId);
        pdfPCell.setBorder(PdfPCell.NO_BORDER);

        return pdfPCell;
    }

    public static PdfPTable createPdfPTable(PdfPCell leftCell, PdfPCell centreCell, PdfPCell rightCell) throws DocumentException {
        PdfPTable footerTable = new PdfPTable(5);

        float[] columnWidths = {34, 34, 0, 34, 0};

        footerTable.setWidths(columnWidths);
        footerTable.addCell(leftCell);
        footerTable.addCell(centreCell);
        footerTable.addCell(rightCell);

        footerTable.setSpacingBefore(25);
        footerTable.setSpacingAfter(25);

        return footerTable;
    }
}
