package ua.dp.levelup.utils;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.EnumMap;
import java.util.Map;

@Service
public class GenerationQRCode {

    @Value("${qrcode.filePath}")
    private String filePath;
    @Value("${qrcode.size}")
    private int size;
    @Value("${qrcode.fileType}")
    private String fileType;

    public void generationQRCode(String urlFromTheTicket, long ticketId) throws WriterException {
        File myFile = new File(filePath, "ticket " + ticketId + ".png");

        try {

            Map<EncodeHintType, Object> hintMap = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
            hintMap.put(EncodeHintType.CHARACTER_SET, "UTF-8");

            hintMap.put(EncodeHintType.MARGIN, 1);
            hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);

            QRCodeWriter qrCodeWriter = new QRCodeWriter();
            BitMatrix byteMatrix = qrCodeWriter.encode(urlFromTheTicket, BarcodeFormat.QR_CODE, size,
                    size, hintMap);
            int CodeWidth = byteMatrix.getWidth();
            BufferedImage image = new BufferedImage(CodeWidth, CodeWidth,
                    BufferedImage.TYPE_INT_RGB);
            image.createGraphics();

            Graphics2D graphics = (Graphics2D) image.getGraphics();
            graphics.setColor(Color.WHITE);
            graphics.fillRect(0, 0, CodeWidth, CodeWidth);
            graphics.setColor(Color.BLACK);

            for (int i = 0; i < CodeWidth; i++) {
                for (int j = 0; j < CodeWidth; j++) {
                    if (byteMatrix.get(i, j)) {
                        graphics.fillRect(i, j, 1, 1);
                    }
                }
            }
            ImageIO.write(image, fileType, myFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("\n\nYou have successfully created QR Code.");
    }

}
