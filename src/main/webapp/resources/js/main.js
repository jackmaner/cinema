window.onclick = function(event) {
    let formContainer = document.getElementById("form-container");
    let modal = document.getElementById('myModal');
    if (checkWhereClicked(event)) {
        formContainer.setAttribute('hidden', '');
        document.getElementById("login-form").setAttribute('hidden', '');
        document.getElementById("auth-form").setAttribute('hidden', '');
    }
    else if (event.target == modal) modal.style.display = "none";
};

function checkWhereClicked(event) {
    let formContainer = document.getElementById("form-container");
    let loginForm = document.getElementById("login-form");
    let authForm = document.getElementById("auth-form");
    let signIn = document.getElementById("sign-in");
    let signIn1 = document.getElementById("sign-in-1");
    let authorization = document.getElementById("authorization");
    let authorization1 = document.getElementById("authorization-1");
    let modal = document.getElementById('myModal');
    if (event.target != formContainer && event.target != signIn &&
        event.target != authorization && event.target != modal &&
        event.target != authorization1 && event.target != signIn1 &&
        event.target != loginForm && event.target != authForm &&
        checkInputClick(event)){
        return true;
    }
    else return false;
}

function checkInputClick(event){
    let formContainer = document.getElementById("form-container");
    let inputs = formContainer.getElementsByTagName('input');
    let result = true;
    for(let i = 0; i < inputs.length; i++){
        if(event.target == inputs[i]) result = false;
    }
    return result;
}

function addLoginForm() {
    if (document.getElementById("login-form").hasAttribute('hidden')) {
        changeForm("auth-form");
        document.getElementById("login-form").removeAttribute('hidden');
    }
    else {
        document.getElementById("form-container").setAttribute('hidden', '');
        document.getElementById("login-form").setAttribute('hidden', '');
    }
    document.getElementById('myModal').style.display = "none";
}
function addAuthForm() {
    if(document.getElementById("auth-form").hasAttribute('hidden')){
        changeForm("login-form");
        document.getElementById("auth-form").removeAttribute('hidden')
    }
    else {
        document.getElementById("form-container").setAttribute('hidden', '');
        document.getElementById("auth-form").setAttribute('hidden', '');
    }
    document.getElementById('myModal').style.display = "none";
}
function changeForm(data) {
    document.getElementById(data).setAttribute('hidden','');
    document.getElementById("form-container").removeAttribute("hidden");
}
function removeForm(){
    document.getElementById("form-container").setAttribute('hidden','');
    document.getElementById("auth-form").setAttribute('hidden', '');
    document.getElementById("login-form").setAttribute('hidden', '');
}

function validateEmail(event){
    event.preventDefault();
    var email = document.getElementById("new-email");
    var emailText = email.value;
    if(emailText.length === 0 || emailText.includes(" ") || emailText.indexOf("@") === -1){
        email.classList.add('red-border');
     }
    else validateLogin;
}

function validateLogin(event){
    event.preventDefault();
    var login = document.getElementById("new-login");
    var loginText = login.value;

    if(loginText.length === 0 || loginText.includes(" ")) login.classList.add('red-border');
    else sendUser;
}

function resetEmailField(){
    document.getElementById("new-email").classList.remove('red-border');
}

function resetLoginField(){
    document.getElementById("new-login").classList.remove('red-border');
}