document.addEventListener("DOMContentLoaded", countPages);
Date.prototype.getFormatDate = function() {
    let monthNames = [ "January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December" ];
    return this.getDate()+' '+monthNames[this.getMonth()]+', '+this.getFullYear()+' ('+(this.getHours()<10?'0':'') + this.getHours()+':'+
        (this.getMinutes()<10?'0':'') + this.getMinutes()+':'+(this.getSeconds()<10?'0':'') + this.getSeconds()+')';
};

function commentsOnPage(data){
    getPageComments(data);
    setTimeout(function(){ window.scrollTo(0, document.getElementById("info-film-block").scrollHeight + 40) }, 400);
}

function showModalWindow(){
    let modal = document.getElementById('myModal');
    let span = document.getElementsByClassName("close")[0];
    modal.style.display = "block";
    span.onclick = function() {
        modal.style.display = "none";
    };
}

function getPageComments(data) {
    cleanPageComments();

    let pageNumber = parseInt(data.innerHTML) - 1;
    let ref = data.href; //полный адрес с contextPath

    fetch(ref, {
        method : "post",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json; charset=utf-8"
        },
        body: JSON.stringify(pageNumber)
    }).then(resp => resp.json())
        .then(data => {
            let comments = [];
            let pageComments = document.getElementById('user-comments');
            let comment = pageComments.firstElementChild;
            let textComment;
            data.forEach(function(item, i, data) {
                let date = new Date(item.dateComment);
                comments[i] = comment.cloneNode(true);
                comments[i].removeAttribute('hidden');
                comments[i].getElementsByClassName('userName-comment')[0].innerHTML = item.user.firstName + " " + item.user.lastName;
                comments[i].getElementsByClassName('date-comment')[0].innerHTML = date.getFormatDate();
                comments[i].getElementsByClassName('text-comment')[0].innerHTML = '';
                textComment = '';
                for(let i = 0; i < item.text.length; i++){
                    if (item.text[i] === '\n'){
                        textComment = textComment + '<br>';
                    }
                    else {
                        textComment = textComment + item.text[i];
                    }
                }
                comments[i].getElementsByClassName('text-comment')[0].innerHTML = textComment;
                pageComments.lastElementChild.appendChild(comments[i]);
            });
        }).catch(error => console.error(error));
}

function countPages() {
    let countCommentsOnFilm = parseInt(document.getElementById('count-comments-on-film').innerHTML);
    let countCommentsOnPage = parseInt(document.getElementById('count-comments-on-page').innerHTML);
    let blockOfPages = document.getElementById('count-pages');
    let pages = [];
    let page = blockOfPages.firstElementChild;
    let countPages = countCommentsOnFilm % countCommentsOnPage === 0
        ? parseInt(countCommentsOnFilm/countCommentsOnPage)
        : parseInt(countCommentsOnFilm/countCommentsOnPage + 1);
    if (countPages === 1 || countCommentsOnFilm === countCommentsOnPage){}
    else {
        for (let i = 0; i < countPages; i++){
            pages[i] = page.cloneNode(true);
            pages[i].removeAttribute('hidden');
            pages[i].firstElementChild.innerHTML = i+1;
            blockOfPages.appendChild(pages[i]);
        }
    }
    getPageComments(page.firstElementChild);
    window.scrollTo(0, 0);
}

function cleanPageComments() {
    document.getElementById('user-comments').lastElementChild.innerHTML = '';
}

function up() {
    //window.scrollBy(0,-10); // чем меньше значение (цифра -10), тем выше скорость перемещения
    //if (window.pageYOffset > 0) {requestAnimationFrame(up);} // если значение прокрутки больше нуля, то функция повториться
    setTimeout(window.scrollTo(0, 120), 2000);

    // window.scrollTo(0, -(document.getElementById("info-film-block").scrollHeight));
    console.log(document.getElementById("info-film-block").scrollHeight);
}