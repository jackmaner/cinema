<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix = "d" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
    <head>
        <title><spring:message code="management-halls.page.list.message"/></title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/css/menu.css"/>
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/css/film.css"/>
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/css/main.css"/>
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/css/hall.css"/>
    </head>
    <body>
        <div class="wrapper">
            <header class="top-column">
                <div>
                    <a href="/" class="logo"><img src="" alt=""/>LOGO</a>
                </div>
                <nav class="head-menu">
                    <ul>
                        <li><a href="page1.html">Страница 1</a></li>
                    </ul>
                </nav>
                <div class="authorization">
                    <d:choose>
                        <d:when test="${auth.email != null}">
                            <a id="auth-user" href="#">${auth.email}</a>
                            <a id="exit-user" href="#">Exit</a>
                        </d:when>
                        <d:otherwise>
                            <a id="sign-in" href="#" class="sign-in" onclick="addLoginForm();">Sign in</a>
                            <a id="authorization" href="${pageContext.servletContext.contextPath}/user/registration" <%--onclick="addAuthForm();"--%>>Registration</a>
                        </d:otherwise>
                    </d:choose>
                </div>
            </header>
            <aside class="left-col">
                <nav>
                    <ul>
                        <li><a href="page1.html">Страница 1</a></li>
                    </ul>
                </nav>
            </aside>
            <main class="right-col">
                <section>
                    <h1 id="hallName"><spring:message code="management-halls.page.list.message"/></h1>
                    <br><br>
                    <form:form id="form-add-rows" action="#" method="post" onsubmit="return createHall()">
                        <div id = "add-hall" class="add-halls" hidden>
                            <table cellspacing="0" style="border:5px;">
                                <thead><!-- <th scope="row"></th> -->
                                    <tr>
                                        <td>hall number</td>
                                        <td>total rows</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><input id="hall-number" type="number" name="hall-number" placeholder="hall-number" maxlength="2" value="1"  min="1" max="99" required></td>
                                        <td><input id="quantity-rows" type="number" name="quantityRowsInHall" placeholder="quantity rows in hall" maxlength="2" value="5"  min="1" max="99" required></td>
                                        <td><input type="submit" id="create-hall" value="Add Hall" form="form-add-rows"/></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </form:form>
                    <form:form class="" action="#" method="post" onsubmit="return createSeats()">
                        <div id="add-rows" hidden>
                            <p>
                                <span>Quantity seats in Row 1 - </span>
                                <input number-row="1" name="row" type="number" placeholder="quantity seats" maxlength="2" value="5"  min="1" max="99" required>
                                <select name="type-row" row="1" size="1">
                                    <option value="COMFORT" selected> COMFORT </option>
                                    <option value="LUX"> LUX </option>
                                </select>
                            </p>
                        </div>
                        <input type="submit" id="create-seats" value="CREATE" hidden/>
                        <input type="button" id="cancelAddHall" value="CANCEL" hidden/>
                    </form:form>
                    <button type="button" id="add-hall-form" onclick="showAddHall()">ADD HALL</button>
                    <button type="button" id="hide-hall-form" onclick="hideHallForm()" hidden>HIDE FORM</button>
                    <div id = "hall-form" style = "margin: 10px 0px 20px 0px">
                        <d:choose>
                            <d:when test="${fn:length(halls) != 0 }">
                                <table cellspacing="0" id ="addUserFormTable">
                                    <thead>
                                    <tr>
                                        <td id="hall-id">HALL ID</td>
                                        <td id="hall-num">HALL NUMBER</td>
                                        <td id="hall-rows">HALL ROWS</td>
                                        <td id="hall-total-seats">TOTAL SEATS IN HALL</td>
                                    </tr>
                                    </thead>
                                    <tbody id="new-user-table">
                                        <d:forEach  var="hall" items="${halls}">
                                            <tr>
                                                <td name="hall-id"> <input type="text" value="${hall.hallId}" disabled> </td>
                                                <td name="hall-num"> <input type="text" value="${hall.hallNumber}" disabled> </td>
                                                <td name="hall-rows"> <input type="text" value="${fn:length(hall.rows)}" disabled> </td>
                                                <fmt:formatNumber var="counts" value="0"/>
                                                <d:forEach  var="row" items="${hall.rows}">
                                                    <fmt:formatNumber var="countSeatsInRow" value="${fn:length(row.seats)}"/>
                                                    <d:set var="counts" value="${counts + countSeatsInRow}"/>
                                                </d:forEach>
                                                <td name="hall-total-seats"> <input type="text" value="${counts}" disabled> </td>
                                            </tr>
                                        </d:forEach>
                                    </tbody>
                                </table>
                            </d:when>
                            <d:otherwise/>
                        </d:choose>
                        </div>
                </section>
                <section></section>
            </main>
            <div id="form-container" class="form-container" hidden>
                <form id="login-form" class="auth-form" action="${pageContext.servletContext.contextPath}/user" method="post" hidden>
                    <input id="userEmail" name="userEmail" type="email" placeholder="email" required>
                    <input id="userPass" name="userPass" type="password" placeholder="password" required>
                    <input type="submit" value="ok">
                </form>
                <form id="auth-form" class="auth-form" action="" hidden>
                    <input type="email" placeholder="email" required>
                    <input type="password" placeholder="password" required>
                    <input type="tel" placeholder="phone" required>
                    <input type="text" placeholder="name" >
                </form>
                <button id="remove-form" id="remove-form" onClick="removeForm()">Cancel</button>
            </div>
            <footer>
                <p>Копирайты</p>
            </footer>
        </div>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/main.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/hall.js"></script>
    </body>
</html>