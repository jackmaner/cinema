<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"  %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="d" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:useBean id="now" class="java.util.Date" scope="page"/>
<html>
    <head>
        <title>Film</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<d:url value='${pageContext.servletContext.contextPath}/resources/css/menu.css' />"/>
        <link rel="stylesheet" type="text/css" href="<d:url value='${pageContext.servletContext.contextPath}/resources/css/film.css' />"/>
        <link rel="stylesheet" type="text/css" href="<d:url value='${pageContext.servletContext.contextPath}/resources/css/main.css' />"/>
        <link rel="stylesheet" type="text/css" href="<d:url value='${pageContext.servletContext.contextPath}/resources/css/modal-widow.css' />"/>
        <link rel="stylesheet" type="text/css" href="<d:url value='${pageContext.servletContext.contextPath}/resources/css/rating.css' />"/>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/main.js"></script>
        <script type="text/javascript" src="<d:url value='${pageContext.request.contextPath}/resources/js/comments/comments.js'/>"></script>
        <script type="text/javascript" src="<d:url value='${pageContext.request.contextPath}/resources/js/comments/sockjs-0.3.js'/>"></script>
        <script type="text/javascript" src="<d:url value='${pageContext.request.contextPath}/resources/js/comments/stomp.js'/>"></script>
    </head>
    <body onload="connect();">
        <div class="wrapper">
            <header class="top-column">
                <div>
                    <a href="/" class="logo"><img src="" alt=""/>LOGO</a>
                </div>
                <nav class="head-menu">
                    <ul>
                        <li><a href="page1.html">Main menu</a></li>
                    </ul>
                </nav>
                <div class="authorization">
                    <d:choose>
                        <d:when test="${auth.email != null}">
                            <a id="auth-user" href="#">${auth.email}</a>
                            <a id="exit-user" href="#">Exit</a>
                        </d:when>
                        <d:otherwise>
                            <a id="sign-in" href="#" class="sign-in" onclick="addLoginForm();">Sign in</a>
                            <a id="authorization" href="${pageContext.servletContext.contextPath}/user/registration" <%--onclick="addAuthForm();"--%>>Registration</a>
                        </d:otherwise>
                    </d:choose>
                </div>
            </header>
            <aside class="left-col">
                <nav>
                    <ul>
                        <li><a href="page1.html">Страница 1</a></li>
                        <li><a href="page2.html">Страница 2</a></li>
                    </ul>
                </nav>
            </aside>
            <main class="right-col">
                <section id="film">
                    <h1>${film.name}</h1>
                    <div class="image-film-block">
                        <p><img width="280" height="400" src="/film/get-film-image/${film.filmId}"/></p>
                        <p>
                            <form action="${pageContext.request.contextPath}/film/process-adding-image/${film.filmId}" enctype="multipart/form-data" method="post">
                                <p><input type="file" name="file">
                                    <input type="submit" value="upload">
                                </p>
                            </form>
                        </p>
                        <p>${message}</p>
                    </div>

                    <%--Rating--%>
                    <div class="stars">
                        <p>Рейтинг фильма:<div id="film-rating"></div></p>
                        <p>Ваша оценка:</p>
                        <span>
                            <label class="star star-5" for="star-5">
                                <img data-id="5" onclick="changeState(this)" class="raiting" src="${pageContext.servletContext.contextPath}/resources/images/star-inactive.jpg">
                            </label>
                            <input class="star star-5" id="star-5" type="radio" name="star"/>
                        </span>
                        <span>
                            <label class="star star-4" for="star-4">
                                <img data-id="4" onclick="changeState(this)" class="raiting" src="${pageContext.servletContext.contextPath}/resources/images/star-inactive.jpg">
                            </label>
                            <input class="star star-4" id="star-4" type="radio" name="star"/>
                        </span>
                        <span>
                            <label class="star star-3" for="star-3">
                                <img data-id="3" onclick="changeState(this)" class="raiting" src="${pageContext.servletContext.contextPath}/resources/images/star-inactive.jpg">
                            </label>
                            <input class="star star-3" id="star-3" type="radio" name="star"/>
                        </span>
                        <span>
                            <label class="star star-2" for="star-2">
                                <img data-id="2" onclick="changeState(this)" class="raiting" src="${pageContext.servletContext.contextPath}/resources/images/star-inactive.jpg">
                            </label>
                            <input class="star star-2" id="star-2" type="radio" name="star"/>
                        </span>
                        <span>
                            <label class="star star-1" for="star-1">
                                <img data-id="1" onclick="changeState(this)" class="raiting" src="${pageContext.servletContext.contextPath}/resources/images/star-inactive.jpg">
                            </label>
                            <input class="star star-1" id="star-1" type="radio" name="star"/>
                        </span>
                    </div>

                    <div id="info-film-block">
                        <div id="film-sessions">
                            <d:forEach var="uDate" items="${uniqueDates}">
                                <fmt:formatDate pattern='dd-MM' type='date' value='${uDate}' var="uniqueDate"/>
                                <p>${uniqueDate}</p>
                                <d:forEach var="session" items="${film.sessionList}">
                                    <fmt:formatDate pattern='dd-MM' type='date' value='${session.sessionStartDate}' var="sessionDate"/>
                                    <fmt:formatDate pattern='HH:mm' type='time' value='${session.sessionStartTime}' var="sessionTime"/>
                                    <d:choose>
                                        <d:when test="${uniqueDate == sessionDate}">
                                            <div style="margin: 10px;">
                                                <p><a href="${pageContext.servletContext.contextPath}/movie/session/${session.movieSessionId}">${sessionTime}</a></p>
                                            </div>
                                        </d:when>
                                        <d:otherwise/>
                                    </d:choose>
                                </d:forEach>
                            </d:forEach>
                        </div>
                        <div id="film-description">
                            ${film.description}
                        </div>
                    </div>
                    <div id="comments-film-block">
                        <h2>Комментарии к фильму: ${film.name}</h2>
                        <d:choose>
                            <d:when test="${(not empty auth.userId)}">
                                <fmt:parseNumber var="userId" integerOnly="true" type="number" value="${auth.userId}"/>
                                <d:set var="firstName" scope="session" value="${auth.firstName}"/>
                                <d:set var="lastName" scope="session" value="${auth.lastName}"/>
                            </d:when>
                            <d:otherwise>
                                <d:if test="${empty auth.userId}">
                                    <fmt:parseNumber var="userId" integerOnly="true" type="number" value="0"/>
                                    <d:set var="firstName" scope="session" value="user"/>
                                    <d:set var="lastName" scope="session" value="user"/>
                                </d:if>
                            </d:otherwise>
                        </d:choose>
                        <div id="new-user-comment">
                            <div>
                                <br />
                                <div id="conversationDiv">
                                    <textarea autofocus id="text" wrap="hard" rows="5" cols="70" maxlength="500" name="new-user-comment" placeholder="your comment" required></textarea><br>
                                    <button id="sendMessage" onclick="sendMessage();">Send</button>
                                </div>
                            </div>
                        </div>
                        <div id="user-comments" class="user-comments">
                            <div class="comment" hidden>
                                <span class="userName-comment">John Doe</span>
                                <span class="date-comment">current Date</span>
                                <p class="text-comment">comment from John Doe</p>
                            </div>
                            <div id="page-comments"></div>
                        </div>
                        <div id="count-pages" class="count-pages">
                            <span class="number-page" hidden><a href="${pageContext.servletContext.contextPath}/film/${film.filmId}" onclick="commentsOnPage(this); return false;">1</a></span>
                        </div>
                        <p id="count-comments-on-film" hidden> <d:out value="${countComments}"/></p>
                        <p id="count-comments-on-page" hidden> <d:out value="${countCommentsOnPage}"/></p>
                    </div>
                </section>
                <div id="authentication"></div>
            </main>
            <div id="form-container" class="form-container" hidden>
                <form:form id="login-form" name="login-form" class="auth-form" action="${pageContext.servletContext.contextPath}/user" method="post" hidden="hidden">
                    <input id="userEmail" name="userEmail" type="email" placeholder="email" required>
                    <input id="userPass" name="userPass" type="password" placeholder="password" required>
                    <input type="submit" value="ok">
                </form:form>
                <form id="auth-form" class="auth-form" action="" hidden>
                    <input type="email" placeholder="email" required>
                    <input type="password" placeholder="password" required>
                    <input type="tel" placeholder="phone" required>
                    <input type="text" placeholder="name" >
                </form>
                <button id="remove-form" id="remove-form" onClick="removeForm()">Cancel</button>
            </div>
            <footer>
                <p>Копирайты</p>
            </footer>
        </div>
        <div id="myModal" class="modal">
            <div class="modal-content">
                <span class="close">&times;</span>
                <p>
                    <span id="failure-message" class="failure-message">In order to comment you must registered.</span> <br>
                    <d:choose>
                        <d:when test="${auth.email != null}">
                            <a id="auth-user" href="#">${auth.email}</a>
                            <a id="exit-user" href="#">Exit</a>
                        </d:when>
                        <d:otherwise>
                            <a href="#" id="sign-in-1" class="sign-in" onclick="addLoginForm();">Sign in</a>
                            <a href="${pageContext.servletContext.contextPath}/user/registration" id="authorization-1" <%--onclick="addAuthForm();"--%>>Registration</a>
                        </d:otherwise>
                    </d:choose>

                </p>
            </div>
        </div>
        <script type="text/javascript">
            var stompClient = null;

            function setConnected(connected) {
                document.getElementById('connect').disabled = connected;
                document.getElementById('disconnect').disabled = !connected;
                document.getElementById('conversationDiv').style.visibility
                    = connected ? 'visible' : 'hidden';
                document.getElementById('response').innerHTML = '';
            }
            function connect() {
                var filmId = ${film.filmId};
                var socket = new SockJS('${pageContext.request.contextPath}/chat-room');
                stompClient = Stomp.over(socket);
                stompClient.connect({}, function(frame) {
                    console.log('Connected: ' + frame);
                    stompClient.subscribe('/topic/comments/'+filmId, function(messageOutput) {
                        showMessageOutput(JSON.parse(messageOutput.body));
                    });
                });
            }
            function disconnect() {
                if(stompClient != null) {
                    stompClient.disconnect();
                }
                setConnected(false);
                console.log("Disconnected");
            }
            function sendMessage() {
                var text = document.getElementById('text').value;
                var filmId = ${film.filmId};
                var userId = ${userId};
                var firstName = '${firstName}';
                var lastName = '${lastName}';

                if(userId === 0){ showModalWindow(); }
                else if (text == ''){
                    document.getElementById('failure-message').innerHTML = 'Comment field mustn\'t be empty!';
                    showModalWindow();
                }
                else {
                    var dateComment = new Date();
                    var film = {filmId};
                    var user = {userId, firstName, lastName};
                    var comment = {'text': text};
                    stompClient.send("/chat-room/" + filmId, {},
                        JSON.stringify({'text': text, 'film': film, 'user': user, 'dateComment': dateComment}));
                }
            }
            function showMessageOutput(messageOutput) {
                let textComment = '';
                let timeMessage = new Date(messageOutput.dateComment);
                let userComments = document.getElementById('user-comments');
                let comment = userComments.firstElementChild;
                let newComment = comment.cloneNode(true);
                newComment.removeAttribute('hidden');
                newComment.getElementsByClassName('userName-comment')[0].innerHTML = messageOutput.user.firstName + " " + messageOutput.user.lastName;
                newComment.getElementsByClassName('date-comment')[0].innerHTML = timeMessage.getFormatDate();
                for(let i = 0; i < messageOutput.text.length; i++){
                    if (messageOutput.text[i] === '\n') textComment = textComment + '<br>';
                    else textComment = textComment + messageOutput.text[i];
                }
                newComment.getElementsByClassName('text-comment')[0].innerHTML = textComment;
                userComments.insertBefore(newComment, userComments.firstChild);
                document.getElementById('text').value='';
            }

            //--Rating

            //document.onload = setUserRateOnPageLoad();
            document.addEventListener("DOMContentLoaded", setUserRateOnPageLoad);
            function setUserRateOnPageLoad() {
                //set average with only 2 nums after dot
                let avgRating = "${averageRating}";
                let parsedavgRating;

                if(avgRating ^ 0 !== avgRating) { //check that num is not an Integer
                    parsedavgRating = Math.floor(avgRating * 10) / 10;
                }else{
                    parsedavgRating = avgRating;
                }

                document.getElementById("film-rating").innerHTML = parsedavgRating;

                //set rating that user setted before
                let usrRate = "${userRate.value}"
                let stars = document.querySelectorAll('img.raiting');

                console.log(stars.length);

                for(let i = stars.length-1; i >= 0; i--) {
                    let star = stars[i];

                    if(usrRate > stars.length-1 - i){
                        star.src = "${pageContext.servletContext.contextPath}/resources/images/star-active.jpg";
                    }
                }
            }

            var lastRaiting = null;
            function changeState(star) {
                var value = star.dataset.id;
                console.log(value);
                setRaiting(value);

                var userId = ${userId};
                var filmId = "${film.filmId}";

                //send user's rate to server
                var sendRateUrl = 'http://localhost:8080/rate/getRating';
                var rateId = ${userRate.rateId}
                    fetch(sendRateUrl, {
                        method : "post",
                        credentials: 'include',

                        headers: {
                            "Accept":"application/json",
                            "Content-Type" : "application/json"
                        },
                        body: JSON.stringify({rateId, value, filmId, userId})
                    })
                        .then(res => res.json())
                        .then(res => {
                            //update average rating after user vote
                            console.log(res)

                            let avgRating = res;
                            let parsedavgRating;

                            if(avgRating ^ 0 !== avgRating) { //check that num is not an Integer
                                parsedavgRating = Math.floor(avgRating * 10) / 10;
                            }else{
                                parsedavgRating = avgRating;
                            }

                            document.getElementById("film-rating").innerHTML = parsedavgRating;
                        })
            }

            function setRaiting(raiting) {
                let stars = document.querySelectorAll('img.raiting');

                for(let i = stars.length-1; i >=0; i--) {
                    let star = stars[i];

                    if(star.dataset.id <= raiting && raiting != lastRaiting) {
                        star.src = "${pageContext.servletContext.contextPath}/resources/images/star-active.jpg";
                        star.setAttribute("checked", true);
                    } else {
                        star.src = "${pageContext.servletContext.contextPath}/resources/images/star-inactive.jpg";
                        star.removeAttribute("checked");
                    }
                }

                if(raiting == lastRaiting) {
                    lastRaiting = null;
                } else lastRaiting = raiting;
            }

            //--Rating

        </script>
    </body>
</html>
