<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page session="true" %>
<html>
<body>

<h1><spring:message code="user.lang.title" />

 <c:choose>
     <c:when test='${cookie.lang.value == null || cookie.lang.value == ""}'>
         <c:out value='en'/>
     </c:when>
     <c:otherwise>
         <c:out value='${cookie.lang.value}'/>
     </c:otherwise>
 </c:choose>

</h1>

<div class="lang-box">
    <span title="English" ><a lang="en" id="en-lang" class="lang" onclick="changeLang(this)" />EN</span>
    <span title="Russian" ><a lang="ru" id="ru-lang" class="lang" onclick="changeLang(this)" />RU</span>
    <span title="Ukrainian" ><a lang="ua" id="ua-lang" class="lang" onclick="changeLang(this)" />UA</span>
</div>

<input hidden type="text" id="session-id" value="${cookie.JSESSIONID.value}" />


<style>
    .selected-lang {
        color: #27cfdb;
        font-weight: bold;
        text-decoration: underline;
    }

    .lang {
        color: gray;
        font-weight: bold;
        text-decoration: underline;
    }

    a.lang:hover {
        color: #996397;
    }
</style>

<script>

    var url = "${pageContext.request.contextPath}/lang/";

    var sessionId = "JSESSIONID=" + document.getElementById("session-id").value;

    var selectedLang = '${cookie.lang.value}' == 0 ? 'en' : '${cookie.lang.value}';

    let langElement = null;

    switch(selectedLang) {
        case 'ru':
            langElement = document.getElementById("ru-lang");
            break;
        case 'ua':
            langElement = document.getElementById("ua-lang");
            break;
        default:
            langElement = document.getElementById("en-lang");
    }

    langElement.classList.remove('lang');
    langElement.classList.add('selected-lang');

    function changeLang(data) {
        let lang = data.firstChild.textContent.trim();
        lang = lang.toLowerCase();
        console.log(url + data.lang);
        setCookie('lang', lang);
        location.reload();
    }

    function setCookie(name, value, options) {
        let list = document.cookie;
        let additionalCookie = name + "=" + value;
        if(list.search(name) < 0) {
            document.cookie = additionalCookie + "; " + document.cookie;
        } else {
            let startIndex = list.indexOf('lang');
            let endIndex = startIndex + additionalCookie.length;
            document.cookie = list.replace(list.slice(startIndex, endIndex), additionalCookie)
        }
    }
</script>

</body>
</html>
