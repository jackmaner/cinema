<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: java
  Date: 04.08.2017
  Time: 20:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css"
          href="<c:url value='${pageContext.request.contextPath}/resources/css/view-basket.css'/>">


    <title>Title</title>
</head>
<body>
<div id="background">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="/">Home</a>
            </div>
            <button class="btn btn-danger navbar-btn" onclick="getBack()">Назад</button>
        </div>
    </nav>
    <%--костыль для получения юзера--%>
    <label class="control-label" for="email">E-mail</label>
    <div class="controls">
        <input type="email" id="email" name="email" placeholder="" class="input-xlarge">
        <p class="help-block">Please provide your E-mail</p>
    </div>
    <%--костыль для получения юзера--%>
    <div align="center" id="main_container" class="container">
        <div class="box">

            <c:forEach var="idOfSeance" items="${seansesIDs}">
            <h3>ID сенса: ${idOfSeance}<br>
                <c:forEach var="ticket" items="${clientTickets.get(idOfSeance)}">
                <div id="ticket${idOfSeance}${ticket.getRowNumber()}${ticket.getSeatNumber()}" class="aa1">
                    <button id="but${idOfSeance}${ticket.getRowNumber()}${ticket.getSeatNumber()}" type="button"
                            class="btn"
                            onclick="deleteTicket(${idOfSeance},${ticket.getRowNumber()},${ticket.getSeatNumber()})"><i
                            id="text${idOfSeance}${ticket.getRowNumber()}${ticket.getSeatNumber()}">удалить</i></button>
                    ряд <c:out value="${ticket.getRowNumber()}"/>
                    , место <c:out value="${ticket.getSeatNumber()}"/>
                    , цена <c:out value="${ticket.getPrice()}"/> ГРН
                    <br>
                </div>
                </c:forEach>
                </c:forEach>
                <p>
                    <button onclick="getUserIdByEmail()" type="button">Купить</button>
                </p>

        </div>
    </div>
</div>

<script type="text/javascript">

    var allTickets = new Map;
    var allTicketsReserved = new Map;
    <c:forEach var="idOfSeance" items="${seansesIDs}">
    <c:forEach var="ticket" items="${clientTickets.get(idOfSeance)}">
    var price = ${ticket.getPrice()};
    var movieSessionId = ${idOfSeance};
    var rowNumber = ${ticket.getRowNumber()};
    var seatNumber = ${ticket.getSeatNumber()};
    var hallId = ${ticket.getHallId()};


    var values = {price, movieSessionId, rowNumber, seatNumber, hallId};
    allTickets.set(${idOfSeance}+${ticket.getRowNumber()}+${ticket.getSeatNumber()}, values);
    allTicketsReserved.set(${idOfSeance}+${ticket.getRowNumber()}+${ticket.getSeatNumber()}, values);
    ${ticket.getRowNumber()}
    </c:forEach>
    </c:forEach>


    function getUserIdByEmail() {
        var email = document.getElementById("email").value;
        var user = {email};
        fetch('${pageContext.request.contextPath}/basket/get-user-id', {
            method : "post",
            headers: {
                "Accept":"application/json",
                "Content-Type" : "application/json"
            },
            body: JSON.stringify(user)
        })
            .then(res => {
                console.log(res);
        })
    .catch(error => console.error(error));

    }

    function buyTickets() {
        var query = [];
        for (let ticket of allTickets.values()) {
            query.push(ticket);
        }
        fetch('${pageContext.request.contextPath}/basket/buy', {
            method: 'POST',
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify(query)
        })
            .catch(alert);
        localStorage.clear();
    }

    function deleteTicket(idOfSeance, row, seat) {
        var tick = document.getElementById('ticket' + idOfSeance + row + seat);
        var text = document.getElementById('text' + idOfSeance + row + seat);
        var btn = document.getElementById('but' + idOfSeance + row + seat);

        text.innerHTML = "вернуть";

        tick.classList.remove("aa1");
        tick.classList.add("aa2");

        btn.setAttribute("onclick", 'abortDeleting(' + idOfSeance + ',' + row + ',' + seat + ')');
        allTickets.delete(idOfSeance + row + seat);
        console.log(allTickets);
        console.log(allTicketsReserved);
    }

    function abortDeleting(idOfSeance, row, seat) {
        var tick = document.getElementById('ticket' + idOfSeance + row + seat);
        var text = document.getElementById('text' + idOfSeance + row + seat);
        var btn = document.getElementById('but' + idOfSeance + row + seat);

        text.innerHTML = "удалить";

        tick.classList.remove("aa2");
        tick.classList.add("aa1");

        btn.setAttribute("onclick", 'deleteTicket(' + idOfSeance + ',' + row + ',' + seat + ')');
        allTickets.set(idOfSeance + row + seat, allTicketsReserved.get(idOfSeance + row + seat));
        console.log(allTickets);
        console.log(allTicketsReserved);
    }

    function getBack() {
        history.back();
    }


</script>
</body>
</html>
