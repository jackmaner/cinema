<!DOCTYPE HTML>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html>
	<head>
		<title><tiles:insertAttribute name="title" ignore="true"/></title>
		<!--

		<meta name="description" content="<tiles:insertAttribute name="page_description" ignore="true"/>">
		<c:url var="cssURL" value="/resources/css/style.min.css"/>
		<link href="${cssURL}" rel="stylesheet" type="text/css"/>
		<link rel="icon" href="<c:url value="/resources/images/favicon.ico"/>" type="image/x-icon" />
		<link rel="shortcut icon" href="<c:url value="/resources/images/favicon.ico"/>" type="image/x-icon" />
		<meta charset="utf-8">
		<meta property="og:image" content="<tiles:insertAttribute name="og_image" ignore="true"/>" />
		<meta property="og:title" content="<tiles:insertAttribute name="og_title" ignore="true"/>" />
		<meta property="og:description" content="<tiles:insertAttribute name="og_desc" ignore="true"/>"/>
		<link rel="stylesheet" href="<tiles:insertAttribute name="jquery_ui_css" ignore="true"/>" />

		-->

		<style>
		    #banner {
		        top: 0px;
		    }

		    #footer_wrapper {
		        bottom: 0px;
                background-color: cornflowerblue;
                width: 100%;
                height: 75px;
                text-align: center;
		    }

		    .lang-box {
		        position: fixed;
                display: block;
                right: 50px;
		    }

		</style>
	</head>
    <body>
    	<div id="banner">
			<tiles:insertAttribute name="header" />
		</div>
		<div><tiles:insertAttribute name="lang_widget" ignore="false"/></div>
		<div></div>
		<tiles:insertAttribute name="navigation_bar" />
		<div></div>
		<div id="page">
			<tiles:insertAttribute name="content" />
		</div>
		<div></div>
		<div id="footer_wrapper">
			<tiles:insertAttribute name="footer" />
		</div>
	</body>
</html>